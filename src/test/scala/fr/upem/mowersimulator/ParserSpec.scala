package fr.upem.mowersimulator
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.GeneratorDrivenPropertyChecks

class ParserSpec extends FlatSpec with Matchers with GeneratorDrivenPropertyChecks {

  "mowerParser" should "return list() if mowerlist is empty" in {
    val mowerlist = List()
    val lawnsize = (5, 5)
    Parser.mowerParser(mowerlist,lawnsize) should be(List())
  }
  it should "return list() if mowerlist match (x,y,NORTH) where x<0 and y<0" in {
    val mowerlist = "-1 -1 N":: Nil
    val lawnsize = (5, 5)
    Parser.mowerParser(mowerlist,lawnsize) should be(List())
  }

  it should "return list() if mowerlist match (x,y,NORTH) where x>lawnsize._1 and/or y>lawnsize._2" in {
    val mowerlist = "6 6 N":: Nil
    val lawnsize = (5, 5)
    val lawnsize2 = (5, 4)
    Parser.mowerParser(mowerlist,lawnsize) should be(List())
    Parser.mowerParser(mowerlist,lawnsize2) should be(List())
  }

  it should "return list() if mowerlist match (x,y,UNKNOWN)" in {
    val mowerlist = "2 2 Z":: Nil
    val lawnsize = (5, 5)
    Parser.mowerParser(mowerlist,lawnsize) should be(List())
  }

  it should "return list() if mowerlist = \"2 2 Qzedf \"" in {
    val mowerlist = "2 2 Qzedf":: Nil
    val lawnsize = (5, 5)
    Parser.mowerParser(mowerlist,lawnsize) should be(List())
  }

  it should "return list() if mowerlist = \"2 2 Qzedf \"" in {
    val mowerlist = "2 2 Qzedf":: Nil
    val lawnsize = (5, 5)
    Parser.mowerParser(mowerlist,lawnsize) should be(List())
  }


  it should "return List() if mowerlist = \"2 2 N\":: Nil" in {
    val mowerlist = "2 2 N" :: Nil
    val lawnsize = (5, 5)
    Parser.mowerParser(mowerlist,lawnsize) should be(List(Mower((2,2),NORTH)))
  }

  it should "return List(Mower((2,4),NORTH)) if mowerlist = \"2 2 N\" :: \"AA\" :: Nil" in {
    val mowerlist = "2 2 N":: "AA" :: Nil
    val lawnsize = (5, 5)
    Parser.mowerParser(mowerlist,lawnsize) should be(List(Mower((2,4),NORTH)))
  }

  it should "return List(Mower((2,4),NORTH)) if mowerlist = \"2 2 N\" :: \"AA\" :: \"222\" :: Nil" in {
    val mowerlist = "2 2 N" :: "AA" :: "222" :: Nil
    val lawnsize = (5, 5)
    Parser.mowerParser(mowerlist, lawnsize) should be(List(Mower((2, 4), NORTH)))
  }
    it should "return List(Mower((2,4),NORTH),Mower((2,4),NORTH),Mower((2,4),NORTH)) if mowerlist = \"2 2 N\" :: \"AA\" ::\"2 2 N\" :: \"AA\" ::\"2 2 N\" :: \"AA\" :: Nil" in {
      val mowerlist = "2 2 N" :: "AA" ::"2 2 N" :: "AA" ::"2 2 N" :: "AA" :: Nil
      val lawnsize = (5, 5)
      Parser.mowerParser(mowerlist,lawnsize) should be(List(Mower((2,4),NORTH),Mower((2,4),NORTH),Mower((2,4),NORTH)))
    }

//////////////////

  "commandParser" should "return Mower((x,y),z) if list=List(ZIOPL), mower = (x,y,z), lawnsize=(a,b)" in {
    val commandList = List('Z','I','O','P','L')
    val mower = (1,1,NORTH)
    val lawnsize = (5,5)
    Parser.commandParser(commandList, mower, lawnsize) should be(Mower((1,1),NORTH))
    }

    it should "return Mower((x,y+2),z) if list=List(ZIOAAL), mower = (x,y,z), lawnsize=(a,b)" in {
      val commandList = List('Z','I','O','A','A','L')
      val mower = (1,1,NORTH)
      val lawnsize = (5,5)
      Parser.commandParser(commandList, mower, lawnsize) should be(Mower((1,3),NORTH))

    }

  it should "return Mower((x,y+1),z) if list=List(A), mower = (x,y,z), lawnsize=(a,b)" in {
    val commandList = List('A')
    val mower = (1,1,NORTH)
    val lawnsize = (5,5)
    Parser.commandParser(commandList, mower, lawnsize) should be(Mower((1,2),NORTH))

  }


  it should "return Mower((x,y),z) if list=List(), mower = (x,y,z), lawnsize=(a,b)" in {
    val commandList = List()
    val mower = (1,1,NORTH)
    val lawnsize = (5,5)
    Parser.commandParser(commandList, mower, lawnsize) should be(Mower((1,1),NORTH))

  }

  it should "return Mower((x,y),NORTH) if list=List(A), mower = (x,y,NORTH), lawnsize=(x,y)" in {
    val commandList = List('A')
    val mower = (2,2,NORTH)
    val lawnsize = (2,2)
    Parser.commandParser(commandList, mower, lawnsize) should be(Mower((2,2),NORTH))

  }

  it should "return Mower((x,y),EAST) if list=List(A), mower = (x,y,EAST), lawnsize=(x,y)" in {
    val commandList = List('A')
    val mower = (2,2,EAST)
    val lawnsize = (2,2)
    Parser.commandParser(commandList, mower, lawnsize) should be(Mower((2,2),EAST))

  }

  it should "return Mower((0,0),SOUTH) if list=List(A), mower = (0,0,SOUTH), lawnsize=(x,y)" in {
    val commandList = List('A')
    val mower = (0,0,SOUTH)
    val lawnsize = (2,2)
    Parser.commandParser(commandList, mower, lawnsize) should be(Mower((0,0),SOUTH))

  }
  it should "return Mower((0,0),WEST) if list=List(A), mower = (0,0,WEST), lawnsize=(x,y)" in {
    val commandList = List('A')
    val mower = (0,0,WEST)
    val lawnsize = (2,2)
    Parser.commandParser(commandList, mower, lawnsize) should be(Mower((0,0),WEST))

  }

  it should "return Mower((x,y=b),z) if list=List(AAAAAAA), mower = (x,y,z), lawnsize=(a,b)" in {
    val commandList = List('A','A','A','A','A','A','A')
    val mower = (1,1,NORTH)
    val lawnsize = (5,5)
    Parser.commandParser(commandList, mower, lawnsize) should be(Mower((1,5),NORTH))

  }

  it should "return Mower((x+1,y+1),EAST) if list=List(AGA), mower = (x,y,z), lawnsize=(a,b)" in {
    val commandList = List('A','D','A')
    val mower = (1,1,NORTH)
    val lawnsize = (5,5)
    Parser.commandParser(commandList, mower, lawnsize) should be(Mower((2,2),EAST))

  }

}