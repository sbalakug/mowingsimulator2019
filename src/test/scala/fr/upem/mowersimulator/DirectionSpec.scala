package fr.upem.mowersimulator

import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.GeneratorDrivenPropertyChecks

class DirectionSpec extends FlatSpec with Matchers with GeneratorDrivenPropertyChecks {

  "Direction" should "return Z if letter or string is different from cardinal points" in {
    val cardinalString = "ZEDSZED"
    val cardinalChar = 'P'
    Process.getDirectionString(cardinalString) should be(UNKNOWN)
    Process.getDirectionChar(cardinalChar) should be(UNKNOWN)
  }

  it should "return NORTH if letter or string is equal to 'N' or \"N\"" in {
    val cardinalString = "N"
    val cardinalChar = 'N'
    Process.getDirectionString(cardinalString) should be(NORTH)
    Process.getDirectionChar(cardinalChar) should be(NORTH)
  }


  it should "return SOUTH if letter or string is equal to 'S' or \"S\"" in {
    val cardinalString = "S"
    val cardinalChar = 'S'
    Process.getDirectionString(cardinalString) should be(SOUTH)
    Process.getDirectionChar(cardinalChar) should be(SOUTH)
  }

  it should "return WEST if letter or string is equal to 'W' or \"W\"" in {
    val cardinalString = "W"
    val cardinalChar = 'W'
    Process.getDirectionString(cardinalString) should be(WEST)
    Process.getDirectionChar(cardinalChar) should be(WEST)
  }

  it should "return EAST if letter or string is equal to 'E' or \"E\"" in {
    val cardinalString = "E"
    val cardinalChar = 'E'
    Process.getDirectionString(cardinalString) should be(EAST)
    Process.getDirectionChar(cardinalChar) should be(EAST)
  }
}
