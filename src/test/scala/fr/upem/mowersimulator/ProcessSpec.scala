package fr.upem.mowersimulator

import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.GeneratorDrivenPropertyChecks

class ProcessSpec extends FlatSpec with Matchers with GeneratorDrivenPropertyChecks{
  "changePosition" should "return (x,y,Direction) if mower = (x,y,Direction), lawnsize = (a,b) and a<x and b<y" in {
    val mower= (2,2,NORTH)
    val lawnsize = (mower._1,mower._2)
    Process.changePosition(mower,lawnsize) should be(mower)
  }

  it should "return (x,y,Direction) if mower = (x*2,y*2,Direction),lawnsize = (a,b) and Direction = UNKNOWN" in {
    val mower= (2,2,UNKNOWN)
    val lawnsize = (mower._1*2,mower._2*2)
    Process.changePosition(mower,lawnsize) should be(mower)
  }




  it should "return (x+1,y,Direction) if mower = (x*2,y*2,Direction),lawnsize = (a,b) and Direction = EAST and x<a" in {
    val mower= (2,2,EAST)
    val lawnsize = (mower._1*2,mower._2*2)
    Process.changePosition(mower,lawnsize) should be((3,2,EAST))
  }
  
  it should "return (x,y,Direction) if mower = (x,y,Direction),lawnsize = (a,b) and Direction = EAST and x=>a" in {
    val mower= (2,2,EAST)
    val lawnsize = (mower._1,mower._2*2)
    Process.changePosition(mower,lawnsize) should be((2,2,EAST))
  }
  
  it should "return (x,y+1,Direction) if mower = (x,y,Direction),lawnsize = (a,b) and Direction = NORTH and y<b" in {
    val mower= (2,2,NORTH)
    val lawnsize = (mower._1*2,mower._2*2)
    Process.changePosition(mower,lawnsize) should be((2,3,NORTH))
  }

  it should "return (x,y,Direction) if mower = (x,y,Direction),lawnsize = (a,b) and Direction = NORTH and y=>b" in {
    val mower= (2,2,NORTH)
    val lawnsize = (mower._1*2,mower._2)
    Process.changePosition(mower,lawnsize) should be((2,2,NORTH))
  }

  it should "return (x,y-1,Direction) if mower = (x,y,Direction),lawnsize = (a,b) and Direction = SOUTH and b>y" in {
    val mower= (2,2,SOUTH)
    val lawnsize = (mower._1*2,mower._2*2)
    Process.changePosition(mower,lawnsize) should be((2,1,SOUTH))
  }

  it should "return (x,y,Direction) if mower = (x,y,Direction),lawnsize = (a,b) and Direction = SOUTH and b<=y" in {
    val mower= (0,0,SOUTH)
    val lawnsize = (2,2)
    Process.changePosition(mower,lawnsize) should be((0,0,SOUTH))
  }
  
  it should "return (x,y-1,Direction) if mower = (x,y,Direction),lawnsize = (a,b) and Direction = WEST and a>x" in {
    val mower= (2,2,WEST)
    val lawnsize = (mower._1*2,mower._2*2)
    Process.changePosition(mower,lawnsize) should be((1,2,WEST))
  }

  it should "return (x,y,Direction) if mower =(x,y,Direction),lawnsize = (a,b) and Direction = EAST and a<=x" in {
    val mower= (0,0,WEST)
    val lawnsize = (2,2)
    Process.changePosition(mower,lawnsize) should be((0,0,WEST))
  }

//////////////////////////////

  "changeDirection" should "return (x,y,UNKNOWN) if mower = (x,y,UNKNOWN)" in {
    val mower= (1,1,UNKNOWN)
    val landsize = (2,2)
    Process.changeDirection(mower,'G',landsize) should be((1,1,UNKNOWN))

  }

  it should "return (x,y,NORTH) if mower = (x,y,NORTH) and command!=[GDA]"in {
    val mower= (1,1,NORTH)
    val landsize = (2,2)
    Process.changeDirection(mower,'Z',landsize) should be((1,1,NORTH))
  }

  it should "return (x,y+1,NORTH) if mower = (x,y,NORTH) and command==[A]"in {
    val mower= (1,1,NORTH)
    val landsize = (2,2)
    Process.changeDirection(mower,'A',landsize) should be((1,2,NORTH))
  }

  it should "return (x,y+1,WEST) if mower = (x,y,WEST) and command==[A]"in {
    val mower= (1,1,WEST)
    val landsize = (2,2)
    Process.changeDirection(mower,'A',landsize) should be((0,1,WEST))
  }

  it should "return (x,y+1,SOUTH) if mower = (x,y,SOUTH) and command==[A]"in {
    val mower= (1,1,SOUTH)
    val landsize = (2,2)
    Process.changeDirection(mower,'A',landsize) should be((1,0,SOUTH))
  }

  it should "return (x,y+1,EAST) if mower = (x,y,EAST) and command==[A]"in {
    val mower= (1,1,EAST)
    val landsize = (2,2)
    Process.changeDirection(mower,'A',landsize) should be((2,1,EAST))
  }



  it should "return (x,y,WEST) if mower = (x,y,NORTH) and command==[G]"in {
    val mower= (1,1,NORTH)
    val landsize = (2,2)
    Process.changeDirection(mower,'G',landsize) should be((1,1,WEST))
  }
  it should "return (x,y,SOUTH) if mower = (x,y,WEST) and command==[G]"in {
    val mower= (1,1,WEST)
    val landsize = (2,2)
    Process.changeDirection(mower,'G',landsize) should be((1,1,SOUTH))
  }

  it should "return (x,y,EAST) if mower = (x,y,SOUTH) and command==[G]"in {
    val mower= (1,1,SOUTH)
    val landsize = (2,2)
    Process.changeDirection(mower,'G',landsize) should be((1,1,EAST))
  }

  it should "return (x,y,NORTH) if mower = (x,y,EAST) and command==[G]"in {
    val mower= (1,1,EAST)
    val landsize = (2,2)
    Process.changeDirection(mower,'G',landsize) should be((1,1,NORTH))
  }


}
