package fr.upem.mowersimulator

object Process {

  //retourne un nouveau couple (Int, Int, Direction) basé sur la Direction actuelle du couple
  def changePosition(x:(Int,Int,Direction), lawnSize:(Int,Int)): (Int, Int, Direction) =  x match {
    case (_,_,NORTH) if x._2>=0 && x._2<lawnSize._2=> (x._1, x._2+1,x._3)
    case (_,_,SOUTH) if x._2>0 && x._2<=lawnSize._2=> (x._1,x._2-1,x._3)
    case (_,_,WEST) if x._1>0 &&x._1<=lawnSize._1=> (x._1-1,x._2,x._3)
    case (_,_,EAST) if x._1>=0 && x._1<lawnSize._1=> (x._1+1,x._2,x._3)
    case (_,_,_)=> (x._1,x._2,x._3)
  }

  //retourne un nouveau couple (Int, Int, Direction) basé sur le contenu dans le char y
  // si y contient 'A', alors on apelle directionChanger pour modifier la position de la tondeuse

  def changeDirection(x:(Int,Int,Direction), y:Char, lawnSize:(Int,Int)) :  (Int,Int,Direction) =
    x match {
      case (_,_,NORTH) if y.equals('G')=> (x._1,x._2,WEST)
      case (_,_,NORTH) if y.equals('D')=> (x._1,x._2,EAST)

      case (_,_,EAST) if y.equals('G')=> (x._1,x._2,NORTH)
      case (_,_,EAST) if y.equals('D')=> (x._1,x._2,SOUTH)

      case (_,_,SOUTH) if y.equals('G')=> (x._1,x._2,EAST)
      case (_,_,SOUTH) if y.equals('D')=> (x._1,x._2,WEST)

      case (_,_,WEST) if y.equals('G')=> (x._1,x._2,SOUTH)
      case (_,_,WEST) if y.equals('D')=> (x._1,x._2,NORTH)

      case (_,_,_) if y.equals('A')=> changePosition((x._1,x._2,x._3),lawnSize)
      case (_,_,_)=> (x._1,x._2,x._3)
    }

  def getDirectionChar(s:Char): Direction = s match {
    case 'N' => NORTH
    case 'E' => EAST
    case 'S' => SOUTH
    case 'W' => WEST
    case  _  => UNKNOWN
  }

  def getDirectionString(s:String): Direction = s match {
    case "N" => NORTH
    case "E" => EAST
    case "S" => SOUTH
    case "W" => WEST
    case  _  => UNKNOWN
  }

}
