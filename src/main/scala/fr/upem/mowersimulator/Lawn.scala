package fr.upem.mowersimulator

//case class représentant une pelouse, contient une liste de tondeuse faite recursivement
//(longeur,hauteur) = (x,y)
//mowers = liste de tondeuses
//Les tondeuses peuvent etre sur la même case de pelouse

case class Lawn(height: Int, width: Int, mowers:List[Mower]) {

  override def toString: String = "Lawn [ width: " + width + ", Height: " + height+ ", Mowers: "+mowers+"]"

}

