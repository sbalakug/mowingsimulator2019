package fr.upem.mowersimulator

sealed trait Direction
case object NORTH extends Direction
case object SOUTH extends Direction
case object WEST extends Direction
case object EAST extends Direction
case object UNKNOWN extends Direction
