package fr.upem.mowersimulator

import scala.io.Source.fromInputStream

object Main extends App{

  val format = "(\\d+) (\\d+).*".r //utilisation d'un pattern pour faciliter le traitement du fichier

  val file = Option(getClass.getResourceAsStream("/input.txt")) //permet d'eviter d'utiliser des throws, trouvé dans le code de MDulac
  file match {
    case None => println("File not found")
    case Some(list) => fromInputStream(list).getLines().toList match {
      case format(s1, s2) :: tail if Integer.parseInt(s1)>1 || Integer.parseInt(s2)>1 => //l'aire du gazon doit etre supérieur a 1 sinon on a une tondeuse qui sert a rien

        val height = Integer.parseInt(s2)
        val width = Integer.parseInt(s1)

        val lawn = Lawn(width, height, Parser.mowerParser(tail,(width,height)))  //(width,height,mowers) //tail = string du reste
        println(lawn)//affichage, les IO sont pas tres code fonctionnel

      case format(s1, s2) :: tail => println("Parsing error, the area should be superior to 1, you have : "+s1+" as width and "+s2+" as height")
      case _ => println("Parsing error, unrecognised values")
    }

  }
}