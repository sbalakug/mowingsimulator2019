package fr.upem.mowersimulator

import scala.annotation.tailrec

object Parser {
  //parseur de tondeuse
  //si tondeuse trouvé, appele commandParser pour parser la ligne suivante(lettres)

  def mowerParser(mowerlist:List[String], lawnsize: (Int,Int)): List[Mower] = {
    val format = "(\\d+) (\\d)+ (N|S|W|E).*".r
    mowerlist match {
      case format(x, y, direction) :: commands :: tail if
      (Integer.parseInt(x) >=0 && Integer.parseInt(x) <= lawnsize._1) &&
      (Integer.parseInt(y) >=0 && Integer.parseInt(y) <= lawnsize._2) &&
      Process.getDirectionString(direction) != UNKNOWN
      => commandParser(commands.toList, (Integer.parseInt(x), Integer.parseInt(y), Process.getDirectionString(direction)), lawnsize) :: mowerParser(tail, lawnsize)

      case format(x, y, direction) :: commands :: Nil  if
      (Integer.parseInt(x) >=0 && Integer.parseInt(x) <= lawnsize._1) &&
        (Integer.parseInt(y) >=0 && Integer.parseInt(y) <= lawnsize._2) &&
        Process.getDirectionString(direction) != UNKNOWN
      => commandParser(commands.toList, (Integer.parseInt(x), Integer.parseInt(y), Process.getDirectionString(direction)), lawnsize) :: Nil

      case format(x, y, direction) :: Nil  if
      (Integer.parseInt(x) >=0 && Integer.parseInt(x) <= lawnsize._1) &&
        (Integer.parseInt(y) >=0 && Integer.parseInt(y) <= lawnsize._2) &&
        Process.getDirectionString(direction) != UNKNOWN
      => Mower((Integer.parseInt(x), Integer.parseInt(y)), Process.getDirectionString(direction)) :: Nil

      case _ => List()
    }
  }

   @tailrec
  def commandParser(list:List[Char], mower:(Int,Int,Direction), lawnSize:(Int,Int)): Mower = list match {
      case letter::tail => commandParser(tail, Process.changeDirection(mower,letter,lawnSize),lawnSize)
      case _ => Mower((mower._1,mower._2),mower._3)
    }
}