package fr.upem.mowersimulator


//Mower, une tondeuse, possede des coordonées (longeur,hauteur), et une direction (N,S,W,E)

case class Mower(coordinates:(Int,Int), direction: Direction) {
    override def toString: String = "[width: "+this.coordinates._1+", height: "+this.coordinates._2+", Direction: "+this.direction+ "]"

}
