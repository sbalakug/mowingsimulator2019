<H1>Projet Scala MowerSimulator
<h4>Par Sivajana Balakuganantham, étudiant en M2 Informatique-Logiciels


<H2>Introduction

Voici donc le projet pour le module Scala de l'année 2018/2019.

Le but de ce projet était de faire un simulateur de tondeuses à gazon automatique pour la société EasyMow destinée aux surfaces rectangulaires.

Le but de cette application est donc de représenter ces tondeuses sur un gazon, les tondeuses ont donc une position initiale, et suivant les informations dans un fichier chargé, doit se situer sur une nouvelle position avec une orientation différente si spécifié

<H2> Comment Lancer le projet

Si c'est ouvert sous intellij, cliquer sur run dans le Main, ou bien tapez run dans le shell sbt. Sinon il suffit de lancer un terminal 

Il est important d'avoir un fichier input.txt contenant les coordonnées dans le dossier ressources, sinon le programme ne sert à rien. Le programme affichera que le fichier n’a pas été trouvé

Voici ce que le fichier input.txt contient de base

    5 5
    1 2 N
    GAGAGAGAA
    3 3 E
    AADAADADDA


<H2> Structure du projet
    
Voici la structure du projet    
    
    build.sbt
    README.md
    Src
        Main
            Ressources
                input.txt
            Scala
                fr.upem.mowersimulator
                    Direction
                    Lawn
                    Main
                    Mower
                    Parser
                    Process
        Test
            Scala
                fr.upem.mowersimulator
                    DirectionSpec
                    ParserSpec
                    ProcessSpec
            


<H2>Choix d'implémentations

<H4>Lawn

Le gazon a deux coordonnées (x, y) ou x et y sont les dimensions du gazon ((5,5) = 5m sur 5m), x représentant la longueur et y la hauteur
Le gazon a aussi une liste de tondeuses

    Lawn   
      (x, y, z) => (Int, Int, List[Mower])

<H4>Mower

Une tondeuse a deux coordonnées (a, b) ou a et b sont entre 0 et (x, y) inclus, et une direction représentée par un point cardinal (Nord, Sud, Ouest, Est)
    
    Mower
        (a, b, c) 
        where a>=0 && a <=x && b>=0 && b<=y 
        && (c.equals(NORTH)  || c.equals(SOUTH) 
        || ||c.equals(WEST) || c.equals(EAST) ) && !c.equals(UNKNOWN)
        => (Int, Int, Direction)

<H4>Direction

La tondeuse possède donc une direction représentée par le sealed trait Direction, qui est étendu par 5 case objects
    
    NORTH, SOUTH, WEST, EAST, UNKNOWN

Unknown est un point cardinal d'erreur, il indique que le fichier a une direction non comprise. Le programme la reconnaitra mais si elle est détectée dans un Mower, alors le Mower est ignoré et le programme s'arrête en retournant l'état actuel de la liste des tondeuses. 

<H4>Fonctionnement

Le programme utilise donc beaucoup de récursivité, en effet c'est un moyen efficace de créer une tondeuse et de traiter sa position avec la contrainte des val, c'est aussi un moyen plus fonctionnel de les représenter et de les traiter. 

Le chargement de fichier se fait avec la fonction getClass.getResourceAsStream, qui a été "emprunté" dans le code de MDulac car je voulais ne pas avoir une erreur (et éviter d'utiliser un throws) si le fichier était absent.


Les affectations sont faites avec Val qui permet l'immutabilité, ainsi que def pour les fonctions.

Lorsque l'on lit notre fichier input.txt, le regex va prendre en compte les deux premiers entiers reconnus de la ligne, ainsi il est possible d'avoir ces exemples
    
    2 2 => (2,2)
    2 2 222256 4 56 4 6548 984 => (2,2)
    2 2 neejnvsr => (2,2)
    2 => println(error)
    1 1 => println(error)
    1 2 => (1,2)

Le programme va créer notre gazon, et va appeler une méthode récursive qui va traiter le reste du fichier

Dans la méthode de traitement de tondeuse, si la méthode ne reconnait pas de coordonnées tondeuse, alors la fonction renvoie une liste des élément reconnu (Vide si premier non-reconnu). La méthode renvoie une liste avec tondeuse sans modification si la suite d'instruction est vide.

S’il n'y a aucun problème, la méthode va alors appeler une autre fonction récursive qui va traiter les commandes

Dans le traitement des commandes, si une lettre est détectée, alors une fonction de traitement de lettre est lancée, elle va permettre de savoir s’il faut faire une rotation ou bien un déplacement. Sinon, la fonction va traiter la prochaine lettre, et s’il n'y a plus de lettres à traiter, alors la méthode renvoie les nouvelles positions de la tondeuse.


<H2>Difficultés du projet 

La première difficulté du projet a été de comprendre le fonctionnement de scala dans un contexte de programmation fonctionnelle.
En effet, pour ce projet, quelques conditions était mise en place afin d'améliorer notre compréhension du langage Scala.

Le fait d'utiliser des val a rendu la tâche assez difficile, en effet, il a fallu créer les objets Mower après les voir "modifié" récursivement.

L’indicateur val a été utilisé pour les fonctions dès le début du projet, malheureusement cela est vite devenu compliquée d'écrire les fonctions, alors d’a été choisi.



<H2>Améliorations possibles

L’utilisation possible des fonctions Apply/unapply pour traiter le fichier pourrait optimiser le programme

Une amélioration de la récursivité pourrait être utile, en effet, il serait possible de mettre en place plus de fonction tail-recursive. 

Un ajout de détection de collision de tondeuse serait un plus

Un ajout de création de fichier de sortie pourrait être intéressant à implémenter.
